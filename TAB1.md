# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Dbt_cloud System. The API that was used to build the adapter for Dbt_cloud is usually available in the report directory of this adapter. The adapter utilizes the Dbt_cloud API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The dbt Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with dbt Cloud. With this adapter you have the ability to perform operations such as:

- List, or Get Projects
- List the runs for a given account
- List Jobs
- Create a Job

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
