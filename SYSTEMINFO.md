# dbt Cloud

Vendor: dbt Labs
Homepage: https://www.getdbt.com/

Product: dbt Cloud
Product Page: https://www.getdbt.com/product/dbt-cloud

## Introduction
We classify dbt Cloud into the Cloud domain as dbt Cloud enables the management and orchestration of data transformation within a cloud-based environment.

"dbt Cloud offers the fastest, most reliable, and scalable way to deploy dbt"

## Why Integrate
The dbt Cloud adapter from Itential is used to integrate the Itential Automation Platform (IAP) with dbt Cloud. With this adapter you have the ability to perform operations such as:

- List, or Get Projects
- List the runs for a given account
- List Jobs
- Create a Job

## Additional Product Documentation
The [API documents for dbt Cloud](https://docs.getdbt.com/docs/dbt-cloud-apis/overview)