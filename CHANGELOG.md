
## 0.3.4 [10-16-2024]

* Changes made at 2024.10.14_19:16PM

See merge request itentialopensource/adapters/adapter-dbt_cloud!10

---

## 0.3.3 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-dbt_cloud!8

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_17:25PM

See merge request itentialopensource/adapters/adapter-dbt_cloud!7

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_18:24PM

See merge request itentialopensource/adapters/adapter-dbt_cloud!6

---

## 0.3.0 [07-23-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/cloud/adapter-dbt_cloud!5

---

## 0.2.3 [03-28-2024]

* Changes made at 2024.03.28_13:05PM

See merge request itentialopensource/adapters/cloud/adapter-dbt_cloud!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_15:25PM

See merge request itentialopensource/adapters/cloud/adapter-dbt_cloud!3

---

## 0.2.1 [02-28-2024]

* Changes made at 2024.02.28_11:34AM

See merge request itentialopensource/adapters/cloud/adapter-dbt_cloud!2

---

## 0.2.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/cloud/adapter-dbt_cloud!1

---

## 0.1.1 [06-26-2023]

* Bug fixes and performance improvements

See commit 4db6e1c

---
